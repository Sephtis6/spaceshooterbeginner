﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
    public static GameManager instance;
    public int score = 0;
    void Awake()
    {
        // As long as there is not an instance already set
        if (instance == null)
        {
            instance = this; 
            DontDestroyOnLoad(gameObject); 
        }
        else
        {
            Destroy(this.gameObject); 
            Debug.Log("Warning: A second game manager was detected and destroyed.");
        }
    }
    //move from the menu screen to the gameplay screen or any other that is added to the build settings
public void LoadByIndex(int sceneIndex)
{
    SceneManager.LoadScene(sceneIndex);
}
//quit the game apllication
public void Quit()
    {
        //if untiy editor
        UnityEditor.EditorApplication.isPlaying = false;
        //else
        Application.Quit();
    }
}
