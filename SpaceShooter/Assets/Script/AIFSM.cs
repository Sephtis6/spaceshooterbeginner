﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIFSM : MonoBehaviour
{

    // Inputs -- What data do we need to make our decisions
    // Position -- so a transform
    public Transform targetTf;
    public Transform tf;
    public float speed;
    private Vector3 movementVector;
    public SpriteRenderer sr;

    // States -- What states can our AI be in
    public enum AIStates { Start, Chase, Stop };
    public AIStates currentState;

    // Use this for initialization
    void Start()
    {
        tf = GetComponent<Transform>();
        targetTf = targetTf.GetComponent<Transform>();
        movementVector = targetTf.position - tf.position;
        sr = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        switch (currentState)
        {
            case AIStates.Start:
                // Do work
                DoStart();
                // Check for Transitions
                if (Vector3.Distance(tf.position, targetTf.position) < 10)
                {
                    currentState = AIStates.Chase;
                }
                if (Vector3.Distance(tf.position, targetTf.position) > 12.5)
                {
                    currentState = AIStates.Stop;
                }
                break;
            case AIStates.Chase:
                // Do Work
                DoChase();
                // Check for transitions
                if (Vector3.Distance(tf.position, targetTf.position) > 15)
                {
                    currentState = AIStates.Stop;
                }
                break;
            case AIStates.Stop:
                DoStop();
                if (Vector3.Distance(tf.position, targetTf.position) < 10)
                {
                    currentState = AIStates.Chase;
                }
                // Check for transitions
                break;
        }
    }

    void DoStart()
    {
        // do nothing
    }

    void DoChase()
    {
        //make the ship red and start to move towards the target
        sr.color = Color.red;
        movementVector = targetTf.position - tf.position;
        tf.right = movementVector;
    }

    void DoStop()
    {
        //makes the ship green when enemy out of range
        sr.color = Color.green;
    }
}
