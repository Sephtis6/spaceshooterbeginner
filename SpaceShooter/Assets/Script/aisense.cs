﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aisense : MonoBehaviour
{

    public float hearingModifier;
    public float fieldOfView;
    public float viewDistance;
    public Transform tf;
    public Transform targetTf;
    public noisemaker nm;
    private Vector3 movementVector;



    // Call this function when AI actively tries to listen to an object
    public bool CanHear(GameObject target)
    {
        // Is that object capable of making noise?
         nm = target.GetComponent<noisemaker>();
        if (nm == null)
        {
            Debug.Log("Can't hear player");
            return false;
        }
        // Is it making noise?
        if (nm.volume <= 0)
        {
            return false;
        }

        // Is the noise loud enough (in other words: Am I close enough)
        // Get distance
        float distance = Vector3.Distance(tf.position,
        targetTf.position);

        // Calculate the modified required distance
        float modDistance = nm.volume * hearingModifier;

        // Check if close enough
        if (distance <= modDistance)
        {
            Debug.Log("Can hear player ship");
            return true;
        }

        // otherwise
        return false;
    }


    public bool CanSee(GameObject target)
    {
        // Field of View
        Vector3 forwardVector = tf.forward;
        Vector3 vectortotarget = targetTf.position - tf.position;

        float angleToTarget = Vector3.Angle(forwardVector, vectortotarget);
        if (angleToTarget <= fieldOfView)
        {
            Debug.Log("Can see player ship");
            return true;
        }
        if (angleToTarget > fieldOfView)
        {
            Debug.Log("Can't see player ship");
            return false;
        }
        // Line of Sight
        RaycastHit2D hitInfo = Physics2D.Raycast(tf.position, vectortotarget, viewDistance);

        if (hitInfo == null)
        {
            Debug.Log("Can't find player ship");
            return false;
        }
        if (hitInfo.collider.gameObject == target)
        {
            Debug.Log("Can see and find player ship");
            return true;
        }
        if (hitInfo.collider.gameObject == target)
        {
            movementVector = targetTf.position - tf.position;
        }

        // Otherwise
        return false;
    }


}
