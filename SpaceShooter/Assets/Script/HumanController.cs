﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanController : Controller {

    public Transform tf;
    public float speed;
    public float turnspeed;

    // Use this for initialization
    void Start()
    {
        tf = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        //move straight
        if (Input.GetKey(KeyCode.W))
        {
            tf.position = tf.position + tf.right;
            //tf.Rotate (0,0,turnspeed);
        }
        //rotate left
        if (Input.GetKey(KeyCode.A))
        {
            //tf.position = tf.position + tf.up;
            tf.Rotate(0, 0, turnspeed);
        }
        //rotate right
        if (Input.GetKey(KeyCode.D))
        {
            // tf.position = tf.position - tf.up;
            tf.Rotate(0, 0, -turnspeed);
        }
        //move backwards
        if (Input.GetKey(KeyCode.S))
        {
            // tf.position = tf.position - tf.right;
            //tf.Rotate (0,0,turnspeed);
        }
        //quit the application
        if (Input.GetKey(KeyCode.Escape))
        {
            UnityEditor.EditorApplication.isPlaying = false;
            Application.Quit();
        }
    }
}
